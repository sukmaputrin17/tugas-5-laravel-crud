
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('template/assets/images/undiksha.png') }}">
    <title>CRUD-Laravel</title>
    <link href="{{ asset('template/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/assets/plugins/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/assets/plugins/chartist-js/dist/chartist-init.css') }}" rel="stylesheet">
    <link href="{{ asset('template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') }}" rel="stylesheet">
    <link href="{{ asset('template/assets/plugins/c3-master/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template//lite/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('template/lite/css/colors/blue.css') }}" id="theme" rel="stylesheet">
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <b>
                            <img src="{{ asset('template/assets/images/undiksha.png') }}" alt="homepage" class="light-logo"/> Data Mahasiswa
                        </b>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> 
                                <a class="srh-btn"><i class="ti-close"></i></a> 
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('template/assets/images/userrr.png') }}" alt="user" class="profile-pic m-r-10" />Sukma Putri</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="/home" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="/insert" aria-expanded="false"><i class="mdi mdi-account-multiple-plus"></i><span class="hide-menu">Tambah Data</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="sidebar-footer">
                <a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
                <a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
        </aside>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Dashboard</a></li>
                            <li class="breadcrumb-item active">Daftar Mahasiswa</li>
                        </ol>
                    </div>
                </div>
                @yield('row')
            </div>
            <footer class="footer">
                © 2021 Create By Sukma
            </footer>
        </div>
    </div>

    <!-- All Jquery -->
    <script src="{{ asset('template/assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/bootstrap/js/tether.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/lite/js/jquery.slimscroll.js') }}"></script>
    <script src="{{ asset('template/lite/js/waves.js') }}"></script>
    <script src="{{ asset('template/lite/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('template/lite/js/custom.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/d3/d3.min.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/c3-master/c3.min.js') }}"></script>
    <script src="{{ asset('template/lite/js/dashboard1.js') }}"></script>
</body>
</html>
