@extends('main')
@section('row')
<div class="row">
    <div class="col-12">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Tabel Mahasiswa</h4>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Mahasiswa</th>
                                <th>NIM Mahasiswa</th>
                                <th>Kelas Mahasiswa</th>
                                <th>Prodi Mahasiswa</th>
                                <th>Fakultas Mahasiswa</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mahasiswa as $showdata)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $showdata->nama_mahasiswa }}</td>
                                <td>{{ $showdata->nim_mahasiswa }}</td>
                                <td>{{ $showdata->kelas_mahasiswa }}</td>
                                <td>{{ $showdata->prodi_mahasiswa }}</td>
                                <td>{{ $showdata->fakultas_mahasiswa }}</td>
                                <td class="text-center">
                                    <a href="{{ url('/update',$showdata->id) }}" style="display:inline" class="btn btn-primary btn-sm">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    &ensp;
                                    <form action="{{ url('/home'.$showdata->id) }}" method="POST" onsubmit="return confirm('HAPUS DATA INI?')" style="display:inline">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection