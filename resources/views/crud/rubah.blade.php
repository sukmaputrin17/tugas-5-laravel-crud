@extends('main')
@section('row')
<div class="row">
    <div class="col-lg-12 col-xlg-9 col-md-7">
        <div class="card">
            <div class="card-block">
                <form action="{{ url('/home',$editingdata->id) }}" method="POST" class="form-horizontal form-material">
                    @method('patch')
                    @csrf
                    <div class="form-group">
                        <label class="col-md-12">Nama Mahasiswa</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Input Your Name!" name="nama" class="form-control form-control-line" required="required" value="{{ $editingdata->nama_mahasiswa }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">NIM Mahasiswa</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Input Your NIM!" name="nim" class="form-control form-control-line" required="required" value="{{ $editingdata->nim_mahasiswa }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Kelas Mahasiswa</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Input Your Class!" name="kelas" class="form-control form-control-line" required="required" value="{{ $editingdata->kelas_mahasiswa }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Prodi Mahasiswa</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Input Your Prodi!" name="prodi" class="form-control form-control-line" required="required" value="{{ $editingdata->prodi_mahasiswa }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Fakultas Mahasiswa</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Input Your Fakultas!" class="form-control form-control-line" name="fakultas" required="required" value="{{ $editingdata->fakultas_mahasiswa }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-success" style="background-color: green">Save Change</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection