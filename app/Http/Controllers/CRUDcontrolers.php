<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use Illuminate\Support\Facades\DB;

class CRUDcontrolers extends Controller
{
    public function show()
    {
     $mahasiswa = DB::table('mahasiswa')->get();
     return view('crud/dashboard', ['mahasiswa'=>$mahasiswa]);
    }



    public function insertdata()
    {
     return view('crud/insert');
    }
    public function insertingdata(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/home')->with('status', 'Data Mahasiswa Berhasil Ditambahkan!');
    }





    public function edit($id)
    {
        $editingdata = DB::table('mahasiswa')->where('id', $id)->first();
        return view('crud/rubah', compact('editingdata'));
    }

    public function editing(Request $request, $id)
    {
        DB::table('mahasiswa')->where('id', $id)
        ->update([
            'nama_mahasiswa'        => $request->nama,
            'nim_mahasiswa'         => $request->nim,
            'kelas_mahasiswa'       => $request->kelas,
            'prodi_mahasiswa'       => $request->prodi,
            'fakultas_mahasiswa'    => $request->fakultas
        ]);
        return redirect('/home')->with('status', 'Data Mahasiswa Berhasil Diubah!');
    }




    public function deleting($id)
    {
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/home')->with('status', 'Data Mahasiswa Telah Dihapus!');
    }
}
