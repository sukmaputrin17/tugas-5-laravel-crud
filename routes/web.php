<?php

use App\Http\Controllers\CRUDcontrolers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [CRUDcontrolers::class, 'show']);

Route::get('/insert', [CRUDcontrolers::class, 'insertdata']);
Route::post('/home',[CRUDcontrolers::class, 'insertingdata']);

Route::get('/update/{id}',[CRUDcontrolers::class, 'edit' ]);
Route::patch('/home/{id}',[CRUDcontrolers::class, 'editing']);

Route::delete('/home{id}',[CRUDcontrolers::class, 'deleting']);